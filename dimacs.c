/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "dimacs.h"

static void
skip_to_eol(FILE *src)
{
  int c;
  while((c=fgetc(src))!=EOF)
    if(c==(int)'\n')
      break;
}

struct clutter*
read_dimacs_dnf(FILE *src, struct clause_ctx *ctx)
{
  struct clutter *res;
  unsigned int numclauses=0, numvars=0;
  int row=0;
  
  /* locate header line */
  while(feof(src)==0) {
    int c=fgetc(src);
    switch(c) {
    case 'c':
      skip_to_eol(src);
      /*@ fallthrough @*/
    case '\n':
      row++;
      continue; /* back to WHILE */
    case 'p':
      if(2!=fscanf(src," dnf %u%u",&numvars,&numclauses)) {
	fprintf(stderr, "row %d: Invalid problem description row (expected 'p dnf <numvars> <numclauses>'\n", row);
	return NULL;
      }
      skip_to_eol(src);
      row++;	
      goto read_terms;
    default:
      fprintf(stderr, "row %d: Unexpected leader char %c\n", row, c);
      return NULL;
    }
  }
 read_terms:
  if(ctx!=NULL && ctx->numbits!=numvars) {
    fprintf(stderr,
	    "Cannot re-use old clause context %p with %zu-bit clauses,"
	    " need %d bits\n", ctx, ctx->numbits, numvars);
    res = NULL;
  } else
    res = make_clutter(numvars, ctx);
    
  if(res!=NULL) {
    unsigned int i;
    for(i=0; i<numclauses && feof(src)==0; i++) {
      struct clause *clause = make_zero_clause(res->clause_ctx);
      unsigned int idx;
      if(clause==NULL) {
	fprintf(stderr, "row %d: cannot allocate clause\n", row);
      } else {
	while((1==fscanf(src,"%u",&idx)) && idx!=0) {
	  if(idx > numvars) 
	    fprintf(stderr, "row %d: Invalid variable index %d ignored\n", row, idx);
	  else
	    clause_set_bit(clause,idx-1);
	}
	if(idx!=0) {
	  /* reading this clause terminated before end of clause */
	  fprintf(stderr, "row %d: clause ends prematurely, last read variable index %d\n", row, idx);
	} else {
	  clutter_store_or_drop_clause(res,clause);
	  skip_to_eol(src);
	  row++;
	}
      }
    }
    
    if(i!=numclauses) {
      fprintf(stderr, "Wrong number of clauses: Found %d, expected %d\n", i, numclauses);
    }
  }

  return res;
}
