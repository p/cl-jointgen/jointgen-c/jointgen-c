/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "config.h"
#include "duality_check.h"
#include <assert.h>
#include <math.h>
#include "debug.h"
#include <gmp.h>


/* check duality when both have size 1 */
static struct certificate*
check_size_1(const struct clutter *A, const struct clutter *B)
{
  const struct clause *ca = A->data[A->live_data->indices[0]];
  const struct clause *cb = B->data[B->live_data->indices[0]];

#if 0 /* This was maybe written a bit too quickly for jointgen-c and
	 fails on B={1,2,3}, A={1}: both size 1, xor={2,3}, yielding
	 {1} for A again later, repeating a clause we already have */
  /* both should be equal unit clauses */
  if((1==clause_popc(A->clause_ctx,ca))
     && clauses_equal(A->clause_ctx,ca,cb)) {
    return answer_dual();
  } else {
    struct clause *x = make_xor_clause(A->clause_ctx, ca, cb, NULL);
    return answer_not_dual(A->clause_ctx, x);
  }
#else /* this is the safe way how cl-jointgen has done it for years: */
  if(clauses_equal(A->clause_ctx,ca,cb)) {
    if(1==clause_popc(A->clause_ctx,ca))
      return answer_dual();
    else {
      /* clauses too large to make a pair */
      struct clause *x = make_xor_clause(A->clause_ctx, ca, cb, NULL);
      return answer_not_dual(A->clause_ctx, x);
    }
  } else {
    /* more could be done here, but we just let the support checker later do
       the heavy lifting for now */
    return answer_undecided();
  }
#endif
}

static struct certificate*
check_same_support(struct clutter *A, struct clutter *B)
{
  struct clause_ctx *ctx = A->clause_ctx;
  
  struct clause *supp_a = clutter_live_support(A, A->scratch_clause);
  struct clause *supp_b = clutter_live_support(B, B->scratch_clause);

  struct clause *vars_missing_in_b = make_andc2_clause(ctx, supp_a, supp_b, NULL);

  if(! clause_zerop(ctx, vars_missing_in_b)) {
    /* need a clause containing IDX in B */
    size_t idx = clause_first_set_bit(ctx,vars_missing_in_b);
    size_t i;
    free_clause(ctx, vars_missing_in_b);
    for(i=0; i<A->live_data->size; i++) {
      const struct clause *src = A->data[A->live_data->indices[i]];
      if(clause_ref(src, idx) == true) {
	struct clause *cert = clause_duplicate(ctx, src);
	clause_clear_bit(cert, idx);
	clause_complement(ctx, cert);
	return answer_not_dual(ctx, cert);
      }
    }
  } else {
    struct clause *vars_missing_in_a = make_andc2_clause(ctx, supp_b, supp_a, NULL);
    free_clause(ctx, vars_missing_in_b);
    if(! clause_zerop(ctx, vars_missing_in_a)) {
      /* need a clause containing IDX in A */
      size_t idx = clause_first_set_bit(ctx,vars_missing_in_a);
      size_t i;
      free_clause(ctx, vars_missing_in_a);
      for(i=0; i<B->live_data->size; i++) {
	const struct clause *src = B->data[B->live_data->indices[i]];
	if(clause_ref(src, idx) == true) {
	  struct clause *cert = clause_duplicate(ctx, src);
	  clause_clear_bit(cert, idx);
	  return answer_not_dual(ctx, cert);
	}
      }
    } else
      free_clause(ctx, vars_missing_in_a);
  }
  return answer_undecided();
}

static struct certificate*
check_too_few_implicants(struct clutter *A, struct clutter *B, bool a_vs_b)
{
  /* check whether A has a larger clause than B has entries */
  size_t i;
  size_t max_popc=0;
  size_t max_idx=0;
  struct clause_ctx *ctx = A->clause_ctx;
  
  for(i=0; i<A->live_data->size; i++) {
    size_t popc = clause_popc(ctx, A->data[A->live_data->indices[i]]);
    if(popc>max_popc) {
      max_popc = popc;
      max_idx = A->live_data->indices[i];
    }
  }
  if(max_popc > B->live_data->size) {
    struct clause *tmp = A->scratch_clause;
    struct clause *large_clause = A->data[max_idx];
    struct clause *cert = make_zero_clause(ctx);

    if(cert) {
      for(i=0; i<B->live_data->size; i++) {
	tmp = make_and_clause(ctx, large_clause,
			      B->data[B->live_data->indices[i]], tmp);
	clause_set_bit(cert, clause_first_set_bit(ctx, tmp));
      }
      if(a_vs_b)
	clause_complement(ctx, cert);
      return answer_not_dual(ctx, cert);
    }
  }
  return answer_undecided();
}

static struct certificate *
compute_clause_by_cond_expectations(struct clutter *A,
				    struct clutter *B,
				    size_t N) /* like in Lemma 1 of FrdKh96 */
{
  struct clause_ctx *ctx = A->clause_ctx;
  struct clause *cert = make_zero_clause(ctx);
  /* space for the population counts of all live clauses of A and B. Space should be reserved in the clutter datastructure */
  size_t* popc_a = malloc(sizeof(size_t) * A->live_data->size);
  size_t* popc_b = malloc(sizeof(size_t) * B->live_data->size);
  size_t* remaining_a = malloc(sizeof(size_t) * A->live_data->size);
  size_t* remaining_b = malloc(sizeof(size_t) * B->live_data->size);

  MAYBE_TRACING(fprintf(stderr, "constructing clause by cond. expectations\n"));
  MAYBE_TRACING(print_clutter(stderr, A, "clutter A is")); 
  MAYBE_TRACING(print_clutter(stderr, B, "clutter B is")); 
  if(popc_a!=NULL && popc_b!=NULL &&
     remaining_a!=NULL && remaining_b !=NULL && cert!=NULL) {
    size_t i;
    size_t remaining_a_cnt, remaining_b_cnt;
    mpz_t part_a, part_b, tmp;
    mpz_t expsum;
    mpz_init2(expsum, N);
    /* expsum := 2^N, the expected sum */
    mpz_set_ui(expsum, 1); mpz_mul_2exp(expsum, expsum, N);

    /* initialize popc */
    for(i=0; i<A->live_data->size; i++) {
      popc_a[i]=clause_popc(ctx,A->data[A->live_data->indices[i]]);
      remaining_a[i]=A->live_data->indices[i];
      MAYBE_TRACING(fprintf(stderr, "A-clause idx %lu, real idx %lu, popc %lu\n",
			    i, A->live_data->indices[i], popc_a[i]));
    }
    for(i=0; i<B->live_data->size; i++) {
      popc_b[i]=clause_popc(ctx,B->data[B->live_data->indices[i]]);
      remaining_b[i]=B->live_data->indices[i];
      MAYBE_TRACING(fprintf(stderr, "B-clause idx %lu, real idx %lu, popc %lu\n",
			    i, B->live_data->indices[i], popc_b[i]));
    }
    remaining_a_cnt=A->live_data->size;
    remaining_b_cnt=B->live_data->size;

    /* for each live variable try whether
       x_i=0 or x_i=1 makes smaller contribution */
    {
      size_t bit;
      mpz_init2(part_a,N); mpz_init2(part_b,N); mpz_init2(tmp,N);
      
      for(bit=0; bit<A->clause_ctx->numbits; bit++) {
	if(bit_masked(A->clause_ctx->mask,bit))
	  continue;
	else {
	  size_t j;
	  MAYBE_TRACING(fprintf(stderr, "bit %lu unmasked\n", bit));
	  mpz_set_ui(part_a,0);
	  /* contribution from a */
	  for(j=0; j<remaining_a_cnt; j++) {
	    /* popc_a is initially the popc, but subsequently corrected to not
	       count the bits where we chose x_i=0. This saves us from shuffling
	       clauses around that have the right intersection with the
	       certificate */
	    mpz_set_ui(tmp,1);
	    mpz_mul_2exp(tmp, tmp, N - popc_a[j]);
	    mpz_add(part_a,part_a,tmp);
	    MAYBE_TRACING(gmp_fprintf(stderr,
				      "A-clause id %d, part_a: +%Zd, now %Zd\n",
				      remaining_a[j], tmp, part_a));
	  }
	  for(j=0; j<remaining_a_cnt; j++) {
	    if(clause_ref(A->data[remaining_a[j]], bit)==true) {
	      /* extra contribution by those in A that have a 1 here */
	      mpz_set_ui(tmp,1);
	      mpz_mul_2exp(tmp, tmp, N - popc_a[j]);
	      mpz_add(part_a,part_a,tmp);
	      MAYBE_TRACING(gmp_fprintf(stderr,
					"A-clause (bit=1) id %d, part_a: +%Zd, now %Zd\n",
				      remaining_a[j], tmp, part_a));
	    }
	  }
	  /* contribution from b */
	  mpz_set_ui(part_b,0);
	  for(j=0; j<remaining_b_cnt; j++) {
	    if(clause_ref(B->data[remaining_b[j]], bit)==false) {
	      mpz_set_ui(tmp, 1);
	      mpz_mul_2exp(tmp, tmp, N - popc_b[j]);
	      mpz_add(part_b, part_b, tmp);
	      MAYBE_TRACING(gmp_fprintf(stderr,
					"B-clause (bit=0) id %d, part_a: +%Zd, now %Zd\n",
					remaining_b[j], tmp, part_b));
	    }
	  }

	  mpz_add(tmp, part_a, part_b);
	  MAYBE_TRACING(gmp_fprintf(stderr, "Need %Zd, have %Zd\n", expsum, tmp));
	  if(mpz_cmp(expsum, tmp)>0) {
	    /** set x_i = 0 **/
	    /* restrict B, correct popc for A (we do not need to modify clauses)  */
	    size_t dst=0;
	    MAYBE_TRACING(fprintf(stderr, "Deciding bit %lu = 0\n", bit));
	    for(j=0; j<remaining_b_cnt; j++) 
	      if(clause_ref(B->data[remaining_b[j]], bit)==false) 
		remaining_b[dst++] = remaining_b[j];
	    remaining_b_cnt=dst;

	    for(j=0; j<remaining_a_cnt; j++) 
	      if(clause_ref(A->data[remaining_a[j]], bit)==true) 
		popc_a[j]--;
	    /* no need to set clause[bit]=0 as that is the init value */
	  } else {
	    /** set x_i = 1 **/
	    /* restrict A, correct popc for B */
	    size_t dst=0;
	    MAYBE_TRACING(fprintf(stderr, "Deciding bit %lu = 1\n", bit));
	    for(j=0; j<remaining_a_cnt; j++) 
	      if(clause_ref(A->data[remaining_a[j]], bit)==false) 
		remaining_a[dst++] = remaining_a[j];
	    remaining_a_cnt=dst;

	    for(j=0; j<remaining_b_cnt; j++) 
	      if(clause_ref(B->data[remaining_b[j]], bit)==true) 
		popc_b[j]--;
	    
	    clause_set_bit(cert,bit);
	  }
	}
      }
    }
    mpz_clear(part_a); mpz_clear(part_b);
    mpz_clear(tmp); mpz_clear(expsum);
    goto DONE;
  } else
    assert(0);

  if(cert!=NULL)
    free_clause(ctx, cert);
 DONE:
  if(popc_a!=NULL) free(popc_a);
  if(popc_b!=NULL) free(popc_b);
  if(remaining_a!=NULL) free(remaining_a);
  if(remaining_b!=NULL) free(remaining_b);
  return answer_not_dual(ctx, cert);
}

static struct certificate*
check_conditional_expectations(struct clutter *A, struct clutter *B)
{
  /* maybe one day we will use mpn type directly */
  struct certificate *res=answer_undecided();
  size_t i;
  mpz_t sum;
  mpz_t tmp;
  size_t N = clutter_num_live_variables(A);
  mpz_t expsum;
  mpz_init2(sum, N); mpz_init2(expsum, N); mpz_init2(tmp, N);
  /* expsum := 2^N, the expected sum */
  mpz_set_ui(expsum, 1); mpz_mul_2exp(expsum, expsum, N);
  
  MAYBE_TRACING(gmp_fprintf(stderr, "Need %Zd, ", expsum));
  /* collect contribution from A */
  for(i=0; i<A->live_data->size; i++) {
    struct clause *c = A->data[A->live_data->indices[i]];
    unsigned long int popc = clause_popc(A->clause_ctx, c);
    mpz_set_ui(tmp,1); mpz_mul_2exp(tmp,tmp,N - popc);
    mpz_add(sum, sum, tmp);
    MAYBE_TRACING(gmp_fprintf(stderr, "A[live[%d]] popc %ld, exp %Zd, sum %Zd\n", i, popc, tmp, sum));
    if(mpz_cmp(sum, expsum)>=0) {
      res = answer_undecided();
      MAYBE_TRACING(gmp_fprintf(stderr, "have %Zd for A\n", sum));
      goto DONE;
    }
  }
  for(i=0; i<B->live_data->size; i++) {
    struct clause *c = B->data[B->live_data->indices[i]];
    unsigned long int popc = clause_popc(B->clause_ctx, c);
    mpz_set_ui(tmp,1); mpz_mul_2exp(tmp,tmp,N - popc);
    mpz_add(sum, sum, tmp);
    MAYBE_TRACING(gmp_fprintf(stderr, "B[live[%d]] popc %ld, exp %Zd, sum %Zd\n", i, popc, tmp, sum));
    if(mpz_cmp(sum, expsum)>=0) {
      res = answer_undecided();
      MAYBE_TRACING(gmp_fprintf(stderr, "have %Zd for A+B\n", sum));
      goto DONE;
    }
  }
  /* sum has not reached 2^N, so we can construct a certificate clause
     as in Lemma 1 of FrdKh96 */
  res=compute_clause_by_cond_expectations(A, B, N);
  
 DONE:
  mpz_clear(tmp);
  mpz_clear(sum);
  mpz_clear(expsum);
  return res;
}

struct certificate*
handle_easy_cases(struct clutter* A, struct clutter *B)
{
  struct clause_ctx *ctx = A->clause_ctx;
  size_t size_a = clutter_count(A);
  size_t size_b = clutter_count(B);

  struct certificate *cert;

  /* we require both contexts to be equal so that all clauses can end
     up either in A or B */
  assert(ctx==B->clause_ctx);

  trace(stderr, "empty A check:\n");
  /* empty A: */
  if(0==size_a) {
    /* check whether B is dual */
    if(0==size_b)
      /* empty edge (for A) is missing */
      return answer_not_dual(ctx, make_zero_clause(ctx));
    else if(has_0_clause(B))
      /* empty A, 0 in B: fine! */
      return answer_dual();
    else
      /* empty A, 0 not in B, let someone decide where 0 belongs */
      return answer_not_dual(ctx, make_zero_clause(ctx));
  }

  trace(stderr, "empty B check:\n");

  /* empty B, nonempty A: */
  if(0==size_b) {
    if(has_0_clause(A)) {
      /* empty B, 0 in A: fine */
      return answer_dual();
    } else {
      /* empty B, nonempty A: decide where 1 belongs */
      return answer_not_dual(ctx, make_allone_clause(ctx));
    }
  }

  trace(stderr, "both size 1 check:\n");
  /* both size 1: */
  if(1==size_a && 1==size_b) {
    cert = check_size_1(A,B);
    if(!DUALITY_UNDECIDED(cert))
      return cert;
  }

  trace(stderr, "same support check:\n");
  /* check for same support */
  cert = check_same_support(A,B);
  if(!DUALITY_UNDECIDED(cert))
      return cert;
  
  trace(stderr, "few implicants:\n");
  /* too few implicants? */
  cert = check_too_few_implicants(A,B, true);
  if(!DUALITY_UNDECIDED(cert))
      return cert;
  
  cert = check_too_few_implicants(B,A, false);
  if(!DUALITY_UNDECIDED(cert))
      return cert;

  trace(stderr, "cond. exp. check:\n");
  /* cond. expectation check */
  /* cert = check_conditional_expectations(A,B); */
  /* if(!DUALITY_UNDECIDED(cert)) */
  /*     return cert; */

  trace(stderr, "easy cases not deciding here.\n");
  /* give up */
  return answer_undecided();
}

struct certificate *
check_duality_recursion(struct clutter *A, struct clutter *B,
			size_t recursion_variable, size_t recursion_depth);

static size_t
decide_frequent_variable(struct clutter *c1, struct clutter *c2)
{
  struct clause_ctx *ctx=c1->clause_ctx;
  size_t num_clauses_a = clutter_count(c1);
  size_t num_clauses_b = clutter_count(c2);
  size_t i;
  size_t candidate;
  size_t candidate_freq=0;


  MAYBE_TRACING(fprintf(stderr, "Freq. var selection: %lu+%lu, candidates: ",
			num_clauses_a, num_clauses_b));
  for(i=0; i<ctx->numbits; i++) {
    if(!bit_unmasked(ctx->mask, i))
      continue; /* should not recurse on masked variables :) */
    if(c1->live_data->varfreq[i] + c2->live_data->varfreq[i] > candidate_freq) {
      candidate_freq = c1->live_data->varfreq[i] + c2->live_data->varfreq[i];
      candidate = i;
      MAYBE_TRACING(fprintf(stderr, "var %lu (%lu) ", candidate, candidate_freq));
    }
  }
  MAYBE_TRACING(fprintf(stderr, "\n"));
  assert (candidate_freq > 1 / log(num_clauses_a+num_clauses_b));
  
  return candidate;
}

struct certificate*
check_duality_helper(struct clutter* A, struct clutter *B, size_t recursion_depth)
{
  struct certificate *cert = handle_easy_cases(A,B);
  if(! DUALITY_UNDECIDED(cert)) {
    assert(ARE_DUAL(cert) || cert->cert!=NULL);
    return cert; /* are_dual or certifying clause */
  } else {
    size_t recursion_variable = decide_frequent_variable(A, B);
    return check_duality_recursion(A,B,recursion_variable,recursion_depth);
  }
}

struct certificate *
check_duality_recursion(struct clutter *A, struct clutter *B,
			size_t recursion_variable, size_t recursion_depth)
{
  struct certificate *cert;
  MAYBE_TRACING(fprintf(stderr, "Recursion depth %lu on variable %lu\n",
			recursion_depth, recursion_variable));
  MAYBE_TRACING(fprintf(stderr, "Mask before: "));
  MAYBE_TRACING(print_clause(stderr, A->clause_ctx, A->clause_ctx->mask,
			     CLAUSE_FORMAT_BITS_LSB));
  
  restrict_clutter_delete(A, recursion_variable);
  restrict_clutter_contract(B, recursion_variable);

  MAYBE_TRACING(fprintf(stderr, "Mask now: "));
  MAYBE_TRACING(print_clause(stderr, A->clause_ctx, A->clause_ctx->mask,
			     CLAUSE_FORMAT_BITS_LSB));
  
  cert = check_duality_helper(A, B, 1+recursion_depth);

  unrestrict_clutter(A, recursion_variable);
  unrestrict_clutter(B, recursion_variable);
  
  if(!ARE_DUAL(cert)) {
    /* have certificate(s), insert variable we recursed on: */
    MAYBE_TRACING(fprintf(stderr, "returning from level %lu var %lu\n",
			  recursion_depth, recursion_variable));
    certificate_set_bit(cert,recursion_variable);
    return cert;
  }

  /* try other leg */
  MAYBE_TRACING(fprintf(stderr, "2nd. leg level %lu var %lu; mask before: ",
			recursion_depth, recursion_variable));
  MAYBE_TRACING(print_clause(stderr, A->clause_ctx, A->clause_ctx->mask,
			     CLAUSE_FORMAT_BITS_LSB));

  restrict_clutter_contract(A, recursion_variable);
  restrict_clutter_delete(B, recursion_variable);

  MAYBE_TRACING(fprintf(stderr, "Mask now: "));
  MAYBE_TRACING(print_clause(stderr, A->clause_ctx, A->clause_ctx->mask,
			     CLAUSE_FORMAT_BITS_LSB));

  cert = check_duality_helper(A, B, 1+recursion_depth);

  unrestrict_clutter(A, recursion_variable);
  unrestrict_clutter(B, recursion_variable);

  if(!ARE_DUAL(cert)) {
    /* have certificate(s), clear variable we recursed on: */
    certificate_clear_bit(cert,recursion_variable);
    return cert;
  } else if(ARE_DUAL(cert)) {
    return answer_dual();
  } else {
    assert(((void)"undecided recursion?",0));
    return answer_undecided();
  }
}







struct certificate*
check_duality(struct clutter* A, struct clutter *B)
{
  return check_duality_helper(A,B,0);
}
