/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "oracle.h"
#include "config.h"
#include <assert.h>
#include "debug.h"

struct oracle*
make_oracle(oracle_fun f, size_t dim, void* closure,
	    struct clause_ctx *ctx,
	    oracle_destructor_fun destructor,
	    clause_min_fun minfun, clause_max_fun maxfun)
{
  struct oracle *res = malloc(sizeof(struct oracle));
  if(res) {
    /* to ensure that if we fail on the next allocation only
       the minimal default deallocation is done */
    res->destructor = NULL;
    res->ctx = ctx!=NULL ? clause_ctx_ref(ctx) : make_clause_ctx(dim);
    if(res->ctx==NULL) {
      free_oracle(res);
      res=NULL;
    } else {
      res->dim = dim;
      res->o = f;
      res->closure = closure;
      res->num_invocations=0;
      res->destructor = destructor;
      res->minfun = minfun !=NULL ? minfun : default_clause_min_fun;
      res->maxfun = maxfun !=NULL ? maxfun : default_clause_max_fun;
    }
  }
  return res;
}

size_t
oracle_get_num_invocations(const struct oracle *o)
{
  return o->num_invocations;
}


void
free_oracle(struct oracle* o)
{
  if(o->destructor)
    o->destructor(o->closure);
  if(o->ctx)
    clause_ctx_deref(o->ctx);
  free(o);
}

enum oracle_answer
query_oracle(struct oracle* oracle, const struct clause* c)
{
  enum oracle_answer res;
  assert(c!=NULL);
  oracle->num_invocations++;
  res=(oracle->o)(oracle, c);
#ifdef TRACE_ORACLE
  fprintf(stderr, "Oracle on ");
  print_clause(stderr, oracle->ctx, c, CLAUSE_FORMAT_BITS_LSB);
  fprintf(stderr, " answers %s\n", res==ORACLE_TRUE ? "YES" : "NO");
#endif

  return res;
}

	 
    
struct clause *
default_clause_min_fun(struct oracle *oracle, struct clause *c)
{
  size_t i;
  for(i=0; i<oracle->ctx->numbits; i++) {
    if(true==clause_ref(c,i)) {
      /* try lowering ith bit */
      clause_clear_bit(c,i);
      if(query_oracle(oracle,c)==ORACLE_FALSE)
	/* reset, it was essential */
	clause_set_bit(c,i);
    }
  }
  return c;
}

struct clause *
default_clause_max_fun(struct oracle *oracle, struct clause *c)
{
  size_t i;
  for(i=0; i<oracle->ctx->numbits; i++) {
    if(false==clause_ref(c,i)) {
      /* try increasing ith bit */
      clause_set_bit(c,i);
      if(query_oracle(oracle,c)==ORACLE_TRUE)
	/* reset, it was essential */
	clause_clear_bit(c,i);
    }
  }
  return c;
}

struct clause*
default_clause_rand_gen_fun(struct oracle *oracle)
{
  /* default: return a random vector */
  struct clause* res = make_zero_clause(oracle->ctx);
  if(res) {
    /* can be improved by harvesting multiple bits per random() call */
    size_t bit;
    for(bit=0; bit<oracle->ctx->numbits; bit++) 
      if(random()-RAND_MAX>0)
	clause_set_bit(res, bit);
  }
  return res;
}

void
minimize_clause(struct oracle *oracle, struct clause *c)
{
  MAYBE_TRACING(fprintf(stderr, "Minimizing clause "));
  MAYBE_TRACING(print_clause(stderr, oracle->ctx, c, CLAUSE_FORMAT_BITS_LSB));
  MAYBE_TRACING(fprintf(stderr, ":\n"));
  
  oracle->minfun(oracle, c);

  MAYBE_TRACING(fprintf(stderr, " yielded "));
  MAYBE_TRACING(print_clause(stderr, oracle->ctx, c, CLAUSE_FORMAT_BITS_LSB));
  MAYBE_TRACING(fprintf(stderr, "\n"));
}
void
maximize_clause(struct oracle *oracle, struct clause *c)
{
  MAYBE_TRACING(fprintf(stderr, "Maximizing clause "));
  MAYBE_TRACING(print_clause(stderr, oracle->ctx, c, CLAUSE_FORMAT_BITS_LSB));
  MAYBE_TRACING(fprintf(stderr, ":\n"));
  
  oracle->maxfun(oracle, c);

  MAYBE_TRACING(fprintf(stderr, " yielded "));
  MAYBE_TRACING(print_clause(stderr, oracle->ctx, c, CLAUSE_FORMAT_BITS_LSB));
  MAYBE_TRACING(fprintf(stderr, "\n"));
  
}
  
