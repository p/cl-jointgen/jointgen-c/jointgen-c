/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "jg.h"
#include "config.h"
#include <assert.h>
#include "oracle.h"

struct jg_prob*
make_jg_prob(struct oracle *oracle)
{
  struct jg_prob *res = malloc(sizeof(struct jg_prob));
  if(res) {
    res->oracle = oracle;
    res->A = make_clutter(oracle->dim, NULL);
    if(res->A) {
      res->B = make_clutter(oracle->dim, res->A->clause_ctx);
      if(!res->B) {
	free_clutter(res->A);
	free(res);
	res=NULL;
      }
    } else {
      free(res);
      res=NULL;
    }
  }
  return res;
}

void
free_jg_prob(struct jg_prob *p)
{
  free_clutter(p->B);
  free_clutter(p->A);
  free_oracle(p->oracle);
  free(p);
}


void
classify_and_store(struct jg_prob *p, struct certificate *c, FILE* trace)
{
  assert(!ARE_DUAL(c));
  assert(!DUALITY_UNDECIDED(c));
  assert(c->cert!=NULL);

  enum oracle_answer res = query_oracle(p->oracle, c->cert);
  switch (res) {
  case ORACLE_TRUE:
    /* if the oracle likes it: make it minimal and store in B */
    MAYBE_TRACING(if(trace) { */
	fprintf(trace,"Oracle accepts: "); 
	print_clause(trace, p->B->clause_ctx, c->cert, CLAUSE_FORMAT_SPARSE); 
      });
    minimize_clause(p->oracle, c->cert);
    MAYBE_TRACING(if(trace) {
	fprintf(trace,"Minimized clause inserted in B: ");
	print_clause(trace, p->B->clause_ctx, c->cert, CLAUSE_FORMAT_SPARSE);
      });
    if(clutter_store_clause(p->B,c->cert)==NULL)
      free(c->cert);
    break;
  case ORACLE_FALSE:
    /* if the oracle rejects it: make it maximal and store complement in A */
    MAYBE_TRACING(if(trace) {
      fprintf(trace,"Oracle rejects: ");
      print_clause(trace, p->A->clause_ctx, c->cert, CLAUSE_FORMAT_SPARSE);
      });
    maximize_clause(p->oracle, c->cert);
    MAYBE_TRACING(if(trace) {
      fprintf(trace,"after maximization oracle still rejects: ");
      print_clause(trace, p->A->clause_ctx, c->cert, CLAUSE_FORMAT_SPARSE);
      });
    clause_complement(p->A->clause_ctx, c->cert);
    MAYBE_TRACING(if(trace) {
      fprintf(trace,"Maximized (and cmplt'd) clause inserted in A: ");
      print_clause(trace, p->A->clause_ctx, c->cert, CLAUSE_FORMAT_SPARSE);
      });
    if(clutter_store_clause(p->A,c->cert)==NULL)
      free(c->cert);
    break;
  default:
    fprintf(stderr, "Oracle invocation failed: %d\n", res);
    break;
  }

  /* if multiple: go on */
  if(c->next!=NULL)
    classify_and_store(p, c->next, trace);
}
    


struct jg_prob*
joint_generation(struct jg_prob *p, FILE* trace)
{
  struct certificate *cert = check_duality(p->A, p->B);

  assert(! DUALITY_UNDECIDED(cert));

  if(ARE_DUAL(cert)) {
    /* are dual, return */
    free_certificate(cert);
    return p;
  } else {
    classify_and_store(p,cert,trace);
    free_certificate(cert);
    /* tail-recurse */
    return joint_generation(p,trace);
  }
}

void
print_jg_prob(FILE *fp, const struct jg_prob *p, const char* header)
{
  /* ad-hoc format: 2 dimacs dnfs with extra header */
  fprintf(fp, "c libjg dual pair\n");
  if(header!=NULL)
    fprintf(fp, "c %s\n", header);
  fprintf(fp, "p jg\n");
  print_clutter(fp, p->A, "maximal false clauses:");
  print_clutter(fp, p->B, "minimal true clauses:");
  fprintf(fp, "c end of jg prob\n");
}
