/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef CLUTTER_H
#define CLUTTER_H

#include "config.h"
#include "clause.h"
#include "bool.h"

struct live_data;

struct live_data {
  /* a vector of NUMBITS entries tabulating the number of occurrences of bit I among
     all clauses currently alive */
  size_t *varfreq;
  /* number of live clauses */
  struct live_data *prev;
  size_t size;
  /* indices of clauses currently alive */   
  size_t indices[1];
};

static struct live_data *
make_live_data(size_t numclauses, size_t numvars)
{
  struct live_data *res = malloc(sizeof(struct live_data) +
				 (sizeof(size_t) * numclauses ));
  if(res) {
    res->size = 0;
    res->prev = NULL;
    res->varfreq = calloc(numvars, sizeof(size_t));
    if(res->varfreq==NULL) {
      free(res);
      res=NULL;
    }
  }
  return res;    
}

static void
free_live_data(struct live_data *d)
{
  free(d->varfreq);
  free(d);
}

static void
free_live_data_recursive(struct live_data *d)
{
  if(d) {
    struct live_data *prev = d->prev;
    free_live_data(d);
    free_live_data_recursive(prev);
  }
}

struct clutter {
  size_t numclauses;
  struct clause_ctx *clause_ctx;
  size_t data_len; 
  struct clause** data;
  /* indices into the above: */
  struct live_data *live_data;
  
  /* to avoid frequent allocations */
  struct clause * scratch_clause;
  
};


/* allocate a new clutter ready to hold clauses of NUMBITS bits. If
   ctx is non-NULL, re-use the clause_ctx for this clutter, otherwise allocate
   a fresh one */
struct clutter *
make_clutter(size_t numbits, /*@shared@*/struct clause_ctx *ctx);

void
free_clutter(struct clutter*c);

void
print_clutter(FILE *fp, const struct clutter *c, const char *header);

static size_t
clutter_count(const struct clutter *c)
{
  return c->live_data->size;
}

static size_t
clutter_num_live_variables(const struct clutter *c)
{
  return clause_popc(c->clause_ctx, c->clause_ctx->mask);
}
/* store clause in clutter. */
struct clutter*
clutter_store_clause_no_check(/*@returned@*/struct clutter *c,
			      /*@kept@*/struct clause *clause);

/* store clause in clutter, which must neither be a superset not a subset of any
   clause in the clutter */
struct clutter*
clutter_store_clause(/*@returned@*/struct clutter *c,
		     /*@kept@*/struct clause *clause);

/* store clause in clutter unless it was too weak, i.e. subsumed by some clause in c. */
struct clutter*
clutter_store_or_drop_clause(/*@returned@*/struct clutter *c,
			     /*@only@*/struct clause *clause);


/*@-temptrans@*/ /*@null@*/
static struct clause*
clutter_live_support(const struct clutter* c, /*@returned@*/ struct clause *dst)
{
  struct clause *tmp = dst!=NULL ? dst : make_clause(c->clause_ctx);
  if(tmp!=NULL) {
    size_t i;
    struct clause_ctx *ctx = c->clause_ctx;
    
    clause_clear(ctx, tmp);
  
    for(i=0; i<c->live_data->size; i++) 
      tmp = make_or_clause(ctx, tmp, c->data[c->live_data->indices[i]],
			   tmp);
    /* clear up to mask */
    for(i=0; i<=ctx->numbits/BITS_PER_SLICE_T; i++) {
      SLICE_T mask = ctx->mask->slice[i];
      tmp->slice[i] &= mask;
    }
  }
  return tmp;

}

bool
has_0_clause(const struct clutter* c);

bool
has_1_clause(const struct clutter* c);

bool
clause_subsumes(const struct clause *clause, struct clutter *c);

bool
clause_subsumed(const struct clause *clause, struct clutter *c);

/* restrict A by selecting only clauses with 0 entries for VARIABLE */
void
restrict_clutter_delete(struct clutter *A, size_t variable);

/* restrict A by ignoring index VARIABLE in all clauses and removing resulting supersets */
void
restrict_clutter_contract(struct clutter *A, size_t variable);

/* undo last restriction on VARIABLE on A */
void
unrestrict_clutter(struct clutter *A, size_t variable);

#endif
