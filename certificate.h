/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef CERTIFICATE_H
#define CERTIFICATE_H

#include "clause.h"
#include "debug.h"
#include <stdio.h>
#include <assert.h>

/********************** Certificates of non-duality: ******************/
/* certificates: possibly multiple certificates returned by one check */
struct certificate;
struct certificate {
  /*@only@*/struct clause* cert;
  /*@null@*//*@only@*/struct certificate *next;
};

/* two certificates that are never valid: */
#ifdef CERTIFICATE_H_IMPL
struct certificate are_dual_certificate;
struct certificate undecided_certificate;
#else
extern struct certificate are_dual_certificate;
extern struct certificate undecided_certificate;
#endif

/* NULL certificate means: dual */
#define ARE_DUAL(cert)          (cert==&are_dual_certificate)
#define DUALITY_UNDECIDED(cert) (cert==&undecided_certificate)

/* construct the YES certificate */
/*@observer@*/
static struct certificate*
answer_dual(void)
{
  return &are_dual_certificate;
}

/* construct the MAYBE certificate */
/*@observer@*/
static struct certificate*
answer_undecided(void)
{
  return &undecided_certificate;
}

/* construct a NO certificate for clause CERT */
/*@out@*//*@null@*/
static struct certificate*
answer_not_dual(struct clause_ctx* ctx, /*@only@*/struct clause* cert)
{
  assert(cert!=0);
  {
    struct certificate* res=malloc(sizeof(struct certificate));
    if(res!=NULL) {
      MAYBE_TRACING(fprintf(stderr, "not dual: "));
      MAYBE_TRACING(print_clause(stderr, ctx, cert, CLAUSE_FORMAT_BITS_LSB));
      MAYBE_TRACING(fprintf(stderr, "\n"));
      res->cert=cert;
      res->next=NULL;
    } else {
      free_clause(ctx,cert);
    }
    return res;
  }
}

/* add another NO certificate NEW_CERT.
   If CERT is NULL, allocate a fresh certificate,
   otherwise prepend NEW_CERT to certificate list */
/*@null@*/static struct certificate*
collect_nonduality_certificate(/*@returned@*/struct certificate* cert,
			       struct clause_ctx *ctx,
			       /*@only@*/struct clause* new_cert)
{
  struct certificate *next = answer_not_dual(ctx, new_cert);
  if(next!=NULL) {
    if(cert!=NULL) {
      cert->next=next;
      return cert;
    } else {
      return next;
    }
  } else {
    /* ignore new_cert */
    return cert;
  }
}

/* destroy certificate, not touching clauses */
static void
free_certificate(struct certificate* cert)
{
  if(ARE_DUAL(cert) || DUALITY_UNDECIDED(cert))
    return;
  else {
    struct certificate *next = cert->next;
    /* cert->cert is assumed to have been consumed by the clutter store or
       explicitly free'ed */
    free(cert);
    if(next!=NULL)
      free_certificate(next);
  }
}

/* set BIT in all certificate clauses: */
static void
certificate_set_bit(struct certificate *cert, size_t bit)
{
  struct clause *c;
  assert(!ARE_DUAL(cert));
  assert(!DUALITY_UNDECIDED(cert));

  do {
    c=cert->cert;
    clause_set_bit(c,bit);
    cert=cert->next;
  } while(cert!=NULL);
}

/* clear BIT in all certificate clauses: */

static void
certificate_clear_bit(struct certificate *cert, size_t bit)
{
  struct clause *c;
  assert(!ARE_DUAL(cert));
  assert(!DUALITY_UNDECIDED(cert));

  do {
    c=cert->cert;
    clause_clear_bit(c,bit);
    cert=cert->next;
  } while(cert!=NULL);
}


#endif
