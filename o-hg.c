/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "oracle.h"
#include "clutter.h"

static
enum oracle_answer
hypergraph_edge_oracle(const struct oracle* o, const struct clause *c)
{
  struct clutter *hg = (struct clutter *)o->closure;
  if(clause_subsumed(c, hg)) 
    return ORACLE_TRUE;
  else
    return ORACLE_FALSE;  
}

static void
hypergraph_destructor(void *closure)
{
  struct clutter *hg = (struct clutter *)closure;
  free_clutter(hg);
}

struct oracle*
make_hypergraph_edge_oracle(struct clutter *c)
{
  return make_oracle(hypergraph_edge_oracle, c->clause_ctx->numbits,
		     c, c->clause_ctx, hypergraph_destructor, NULL, NULL);  
}
