/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "clause.h"

struct clause_ctx*
make_clause_ctx(size_t numbits)
{
  struct clause_ctx *res = malloc(sizeof(struct clause_ctx));
  if(res!=NULL) {
    res->numbits=numbits;
    res->clause_size = sizeof(struct clause) +
      (numbits/BITS_PER_SLICE_T)*sizeof(SLICE_T);
    res->numrefs=1;
    res->mask = make_allone_clause(res);
    if(res->mask==NULL) {
      clause_ctx_deref(res);
      res=NULL;
    }
    return res;
  } else
    return NULL;
}

#define clause_popc_slices32 clause_popc

size_t
clause_popc_loop(struct clause_ctx* ctx, const struct clause *c)
{
  /* naive implementation */
  size_t popc=0;
  size_t i;							
  for(i=0; i<=(ctx)->numbits/BITS_PER_SLICE_T; i++) {
    size_t j;
    SLICE_T mask = ctx->mask->slice[i];
    for(j=0; j<BITS_PER_SLICE_T; j++) {
      popc += (c->slice[i] & mask & ((SLICE_T)1<<j)) == 0 ? 0 : 1;
    }
  }
  return popc;
}

static size_t
popc_slice(SLICE_T x)
{
  size_t count = 0;
  size_t i;
  
  for(i=0; i<sizeof(SLICE_T)/sizeof(uint32_t); i++) {
    uint32_t word = x & 0xffffffff;

    word -= ((word >> 1) & 0x55555555);
    word = (((word >> 2) & 0x33333333) + (word & 0x33333333));
    word = (((word >> 4) + word) & 0x0f0f0f0f);
    word += (word >> 8);
    word += (word >> 16);
    count += word & 0x0000003f;
    x = x >> 32;
  }
  return count;
}

size_t
clause_popc_slices32(struct clause_ctx* ctx, const struct clause *c)
{
  size_t popc=0;
  size_t i;
  for(i=0; i<=ctx->numbits/BITS_PER_SLICE_T; i++)
    popc += popc_slice(c->slice[i] & ctx->mask->slice[i]);

  return popc;
}

void
print_clause(FILE *fp, struct clause_ctx *ctx, const struct clause *c,
	     enum clause_print_format fmt)
{
  switch (fmt) {
  case CLAUSE_FORMAT_SPARSE: {
    size_t bit;
    for(bit=0; bit<ctx->numbits; bit++) {
      if (clause_ref(c, bit)==true) {
	bool masked=false;
	if(clause_ref(ctx->mask,bit)==false)
	  masked=true;
	fprintf(fp, "%s%lu%s ",
		masked ? "(" : "",
		(1+bit),
		masked ? ")" : "");
      }
    }
    fprintf(fp, "0\n");
    break;
  }
  case CLAUSE_FORMAT_BITS_LSB: {
    size_t bit;
    fprintf(stderr, "#*"); /* lisp-like prefix */
    for(bit=0; bit<ctx->numbits; bit++) {
      bool bitval = clause_ref(c, bit);
      fprintf(fp, "%c", bit_masked(ctx->mask,bit) ?
	      '.' : ((bitval==true) ? '1' : '0'));
    }
    break;
  }
  case CLAUSE_FORMAT_BITS_MSB: {
    size_t bit;
    fprintf(stderr, "#*"); /* lisp-like prefix */
    for(bit=ctx->numbits; bit-->0; /*nothing*/) {
      bool bitval = clause_ref(c, bit);
      fprintf(fp, "%c", bit_masked(ctx->mask, bit) ? 
	      '.' : ((bitval==true) ? '1' : '0'));
    }
    break;
  }
  default:
    fprintf(stderr, "Unrecognized clause print format %d\n", fmt);
  }
  
}
