/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef JG_H
#define JG_H

#include "oracle.h"
#include "clutter.h"
#include "certificate.h"
#include "duality_check.h"
#include <stdio.h>

struct jg_prob {
  struct clutter *A;
  struct clutter *B;
  struct oracle *oracle;
};

/* Consumes oracle */
struct jg_prob*
make_jg_prob(struct oracle *oracle);

void
free_jg_prob(struct jg_prob *p);

void
print_jg_prob(FILE *fp, const struct jg_prob *p, const char* header);

struct jg_prob*
joint_generation(struct jg_prob *p, FILE* trace);

#endif
