/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "oracle.h"
#include "debug.h"

static void
store_permutation(struct clutter *c, size_t dim, const size_t *perm)
{
  struct clause *clause = make_zero_clause(c->clause_ctx);
  if(clause!=NULL) {
    size_t i;
    for(i=0; i<dim; i++) {
      size_t j = perm[i]; 
      size_t idx = i*dim + j;
      clause_set_bit(clause, idx);
    }
  }
  clutter_store_clause(c, clause);
}

static struct clutter *
make_permutation_clutter(size_t dim)
{
  /* Heap's method, see Sedgwick's '77 paper */
  struct clutter *res = make_clutter(dim*dim, NULL);
  size_t *perm = malloc(sizeof(size_t) * dim);
  size_t *c = malloc(sizeof(size_t) * dim);
  
  if(res!=NULL && perm!=NULL && c!=NULL) {
    size_t i=0;
    /* unit permutation and intial c vector */
    for(i=0; i<dim; i++) {
      perm[i]=i;
      c[i]=0;
    }

    store_permutation(res, dim, perm);
    i=1;
    do {
      size_t k;
      
      if(c[i]<i) {
	if(i%2 == 0) /* odd/even */
	  k=0;
	else
	  k=c[i];
	{
	  size_t tmp = perm[i];
	  perm[i]=perm[k];
	  perm[k]=tmp;
	}

	c[i]++;
	i=1;

	store_permutation(res, dim, perm);
      } else {
	c[i]=0;
	i++;
      }	  
    } while(i<dim);
  }
  if(perm!=NULL)
    free(perm);
  if(c!=NULL)
    free(c);

  print_clutter(stderr, res, "perms: ");
  return res;
}


struct oracle*
make_permutation_oracle(size_t dim)
{
  struct clutter *perms = make_permutation_clutter(dim);
  struct oracle *o = make_hypergraph_edge_oracle(perms);
  return o;
}
