/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef CLAUSE_H_
#define CLAUSE_H_ 1

#include "config.h"

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include <stdio.h>
#include "bool.h"
#include <assert.h>

/* A clause is nothing but a chunk of memory, made up of SLICE_T pieces */

#ifdef USE_64BIT_SLICES
#define SLICE_T uint64_t
#define BITS_PER_SLICE_T ((size_t)64UL)
#else
#define SLICE_T uint32_t
#define BITS_PER_SLICE_T ((size_t)32UL)
#endif

struct clause {
  SLICE_T slice[1]; /* dynamically extended struct */
};

/* A clause context is keeping allocation information and statistics for a bunch of clauses */
/*@refcounted@*/
struct clause_ctx {
  /* number of bits in a clause */
  size_t numbits;
  size_t clause_size;
  /*@refs@*/
  size_t numrefs;
  struct clause *mask; /* mask, 1-bits indicating live bits of clauses in this context */
  /* could be extended to use a clever allocation strategy or allocator
     library */
};

/*@null@*/struct clause_ctx *
make_clause_ctx(size_t numbits);

/* destroy clause */
/*@unused@*/
static void
free_clause(/*@unused@*/ struct clause_ctx* ctx,
	    /*@only@*/ /*@out@*/ /*@null@*/ struct clause* c)
{
  free(c);
}


/* reference counting: */
static struct clause_ctx *
clause_ctx_ref(struct clause_ctx *ctx) {
  ctx->numrefs++;
  return ctx;
}

static struct clause_ctx *
clause_ctx_deref(struct clause_ctx *ctx) {
  ctx->numrefs--;
  if(ctx->numrefs==0) {
    if(ctx->mask)
      free_clause(ctx,ctx->mask);
    free(ctx);
    return NULL;
  } else
    return ctx;
}


/* allocate a new clause */
/* we ensure that unused bits are always 0 */
/*@null@*//*@out@*/ static struct clause * 
make_clause(struct clause_ctx* ctx)
{
  struct clause *res = malloc(ctx->clause_size);
  /* clear top word */
  if(res!=NULL)
    res->slice[ctx->numbits/BITS_PER_SLICE_T] = (SLICE_T) 0;
  return res;
}

static struct clause *
clause_duplicate(struct clause_ctx *ctx, const struct clause* c)
{
  struct clause *res = make_clause(ctx);
  memcpy(res->slice,c->slice,ctx->clause_size);
  return res;
}

enum clause_print_format {
  CLAUSE_FORMAT_SPARSE,
  CLAUSE_FORMAT_BITS_LSB,
  CLAUSE_FORMAT_BITS_MSB
};

#define DEFAULT_CLAUSE_PRINT_FORMAT   CLAUSE_FORMAT_SPARSE

void
print_clause(FILE *fp, struct clause_ctx *ctx, const struct clause *c,
	     enum clause_print_format fmt);

size_t
clause_popc(struct clause_ctx* ctx, const struct clause *c);

static void
clause_clear(struct clause_ctx *ctx, /*@temp@*/ struct clause *c)
{
  if(1==HAVE_MEMSET)
    memset(c->slice,0,ctx->clause_size);
  else {
    size_t i;
    for (i=0; i<=ctx->numbits/BITS_PER_SLICE_T; i++) 
      c->slice[i] = 0;
  }
}

/* allocate a new all-0 clause */
/*@unused@*/
/*@null@*//*@out@*/ static struct clause *
make_zero_clause(struct clause_ctx* ctx)
{
  struct clause *res = make_clause(ctx);
  if(res!=NULL) {
    /*@-compdef@*/ clause_clear(ctx, res);
    return res;
  } else
    return res;
}

#define CLEAR_TOP_BITS(ctx,c)						\
  do {									\
    SLICE_T valid_top_bits = ((SLICE_T)1<<((ctx)->numbits%BITS_PER_SLICE_T)) -1; \
    (c)->slice[(ctx)->numbits/BITS_PER_SLICE_T] &= valid_top_bits;	\
  } while(false);

/* allocate a new all-0 clause */
/*@unused@*/ /*@null@*/ /*@out@*/
static struct clause *
make_allone_clause(struct clause_ctx* ctx)
{
  struct clause *res = make_clause(ctx);

  if(res!=NULL) {
    if(1==HAVE_MEMSET)
      memset(res->slice,~((int)0),ctx->clause_size);
    else {
      size_t i;
      for (i=0; i<=ctx->numbits/BITS_PER_SLICE_T; i++) 
	res->slice[i] = ~((SLICE_T)0U);
    }
    CLEAR_TOP_BITS(ctx,res);
  }
  return res;
}


/* operations on clauses */
/*@unused@*/
static bool
clause_ref(const struct clause *c, size_t bit)
{
  size_t slice = bit/BITS_PER_SLICE_T;
  size_t slicebit = ((SLICE_T)1) << (bit%BITS_PER_SLICE_T);
  /*   fprintf(stderr, "bit %u, slice %u, slicebit %u, data %u, val %u, returning %d\n", */
  /* 	  bit, slice, slicebit, c->slice[slice], c->slice[slice] & slicebit, */
  /* 	  0 == (c->slice[slice] & slicebit) ? false : true); */
  if(0 == (c->slice[slice] & slicebit))
    return false;
  else
    return true;
}

/*@unused@*/
static struct clause*
clause_set_bit(/*@returned@*/struct clause *c, size_t bit)
{
  c->slice[bit/BITS_PER_SLICE_T] |= ((SLICE_T)1 << (bit%BITS_PER_SLICE_T));
  return c;
}

/*@unused@*/
static struct clause*
clause_clear_bit(/*@returned@*/struct clause *c, size_t bit)
{
  c->slice[bit/BITS_PER_SLICE_T] &= ~((SLICE_T)1 << (bit%BITS_PER_SLICE_T));
  return c;
}

static void
clause_ctx_mask_bit(struct clause_ctx *ctx, size_t bit) {
  clause_clear_bit(ctx->mask,bit);
}

static void
clause_ctx_unmask_bit(struct clause_ctx *ctx, size_t bit) {
  clause_set_bit(ctx->mask,bit);
}

static bool
bit_masked(const struct clause *c, size_t bit)
{
  return clause_ref(c,bit) == false;
}

static bool
bit_unmasked(const struct clause *c, size_t bit)
{
  return clause_ref(c,bit) == true;
}


/* Complement clause in-place. Does not take mask into account. */
/*@unused@*/
static struct clause*
clause_complement(struct clause_ctx *ctx, /*@returned@*/struct clause *c)
{
  size_t i;
  SLICE_T valid_top_bits;
/*   fprintf(stderr, "Complementing "); */
/*   print_clause(stderr, ctx, c, CLAUSE_FORMAT_SPARSE); */
  
  for(i=0; i<(ctx)->numbits/BITS_PER_SLICE_T; i++) {	       
    c->slice[i] = ~c->slice[i];
  }
  /* top bits need to be handled properly to keep zeros */
  valid_top_bits = ((SLICE_T)1<<(ctx->numbits%BITS_PER_SLICE_T)) - 1;
  c->slice[i] = (~c->slice[i]) & valid_top_bits;

/*   fprintf(stderr, " yields "); */
/*   print_clause(stderr, ctx, c, CLAUSE_FORMAT_SPARSE); */
/*   fprintf(stderr, "\n"); */

  return c;

}
	

/*@unused@*/
static bool
clauses_equal(const struct clause_ctx *ctx,
	      const struct clause *c1,
	      const struct clause *c2)
{
  size_t i;
  for(i=0; i<=(ctx)->numbits/BITS_PER_SLICE_T; i++) {
    SLICE_T mask = ctx->mask->slice[i];
    if((c1->slice[i]&mask) != (c2->slice[i]&mask))
      return false;
  }
  return true;
}
 


/*@unused@*/
static bool
clause_zerop(const struct clause_ctx *ctx, const struct clause *c)
{
  size_t i;
  for(i=0; i<ctx->clause_size/sizeof(SLICE_T); i++) {
    SLICE_T mask = ctx->mask->slice[i];
    if((c->slice[i]&mask) != 0)
      return false;
  }
  return true;
}

/*@unused@*/
static bool
clause_all_onep(const struct clause_ctx *ctx, const struct clause *c)
{
  size_t i;
  for(i=0; i<ctx->clause_size/sizeof(SLICE_T); i++) {
    SLICE_T mask = ctx->mask->slice[i];
    if((c->slice[i]&mask) != mask)
      return false;
  }
  return true;
}

/* do bitwise operation on all slices: */
#define FOR_EACH_SLICE(ctx,res,ca,cb,bitop)				\
  do {									\
    size_t i;								\
    for(i=0; i<=(ctx)->numbits/BITS_PER_SLICE_T; i++) {			\
      SLICE_T mask = (ctx)->mask->slice[i];				\
      (res)->slice[i] = ((ca)->slice[i] bitop (cb)->slice[i]) & mask;	\
    }									\
  } while(false)


/***************************************************************
  Bitoperations on clauses. Convention: operation on A OP B, result into DST if
  that was non-NULL, otherwise allocate fresh clause.
*/

#define CLAUSE_BITOP_DEF(fun,op)                                \
  /*@unused@*/							\
  static struct clause *					\
  fun(struct clause_ctx *ctx,					\
      const struct clause* a,					\
      const struct clause* b,					\
      /*@returned@*/struct clause* dst)				\
  {								\
    struct clause* res = dst!=NULL ? dst : make_clause(ctx);	\
    FOR_EACH_SLICE(ctx,res,a,b,op);				\
    return res;							\
  }

CLAUSE_BITOP_DEF(make_xor_clause,^)

CLAUSE_BITOP_DEF(make_or_clause,|)

CLAUSE_BITOP_DEF(make_and_clause,&)

/* C1 And NOT(C2). Works as though mask is not there. */
/*@unused@*/					
static struct clause *				
make_andc2_clause(struct clause_ctx *ctx,	
		  const struct clause* c1,	
		  const struct clause* c2,	
		  /*@returned@*/struct clause* dst)
{							
  struct clause* res = dst!=NULL ? dst : make_clause(ctx);
  size_t i;

  for(i=0; i<=(ctx)->numbits/BITS_PER_SLICE_T; i++) {		
    res->slice[i] = c1->slice[i] & (~(c2->slice[i]));	
  }
  /* no need to clean top bits as c1 has zeros there */
  return res;				
}
/* C2 And NOT(C1) */
/*@unused@*/					
static struct clause *				
make_andc1_clause(struct clause_ctx *ctx,	
		  const struct clause* c1,	
		  const struct clause* c2,	
		  /*@returned@*/struct clause* dst)
{
  return make_andc2_clause(ctx,c2,c1,dst);
}


/* check whether C1 contains all bits of C2 */
static bool
clause_contains(struct clause_ctx *ctx,
		const struct clause *c1,
		const struct clause *c2)
{
  size_t i;
  const SLICE_T *s1 = c1->slice;
  const SLICE_T *s2 = c2->slice;
  const SLICE_T *mask = ctx->mask->slice;

  for(i=0; i<=(ctx)->numbits/BITS_PER_SLICE_T; i++) {
    SLICE_T common_bits = s1[i] & s2[i] & mask[i];
    if(common_bits != (mask[i] & s2[i]))
      return false;
  }
  return true;  
}

/* check whether C1 contains all bits of C2 */
static bool
clause_contained(struct clause_ctx *ctx,
		 const struct clause *c1,
		 const struct clause *c2)
{
  return clause_contains(ctx,c2,c1);
}

/* return index of first non-0 bit in C. C must have a non-0 bit */
static size_t
clause_first_set_bit(struct clause_ctx *ctx, const struct clause *c)
{
  size_t i;
  for(i=0; i<ctx->numbits; i++) {
    if(bit_unmasked(ctx->mask, i) && clause_ref(c, i)==true)
      return i;
  }
  assert(false);
  return 0;
}

#endif
