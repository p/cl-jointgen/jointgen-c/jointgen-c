/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "clutter.h"
#include "debug.h"
#include <assert.h>
#include <stdio.h>

#define NFREE(foo) do { if(foo!=NULL) free(foo); } while(false)
#define NFREEN(foo) do { if(foo!=NULL) { free(foo); foo=NULL} ; } while(false)

/* clutter datastructure should provide infrastructure for 
 * reporting progress, growth, added clauses etc. For now, for
 * debugging, we sometimes use this:
 */
#if 0
#define maybe_report(clutter) do {		\
	fprintf(stdout, ".");			\
	fflush(stdout);				\
	} while(0);
#else
#define maybe_report(clutter) do { } while(0)
#endif

struct clutter *
make_clutter(size_t numbits, struct clause_ctx *ctx)
{
  struct clutter *res = malloc(sizeof(struct clutter));
  if(res!=NULL) {
    res->numclauses=0;
    res->data=NULL;
    res->data_len=0;
    res->live_data = make_live_data(0,numbits);
      
    if(ctx!=NULL) {
      assert(ctx->numbits==numbits);
      res->clause_ctx = clause_ctx_ref(ctx);
    } else {
      res->clause_ctx = make_clause_ctx(numbits);
      if(!res->clause_ctx) {
	free(res);
	res=NULL;
      }
    }

    res->scratch_clause = make_clause(res->clause_ctx);
    if(!res->scratch_clause || !res->live_data) {
      free_clutter(res);
      res=NULL;
    }
  }
  return res;
}

void
free_clutter(struct clutter *c)
{
  size_t i;
  for(i=c->numclauses; i>0; i--)
    free_clause(c->clause_ctx, c->data[i-1]);
  if(c->data)
    free(c->data);
  if(c->scratch_clause)
    free_clause(c->clause_ctx, c->scratch_clause);
  
  free_live_data_recursive(c->live_data);

  clause_ctx_deref(c->clause_ctx);
  
  free(c);
}



bool
has_0_clause(const struct clutter *c)
{
  size_t i;
  for(i=0; i<c->live_data->size; i++) {
    if(true==clause_zerop(c->clause_ctx,
			  c->data[c->live_data->indices[i]]))
      return true;
  }
  return false;
}

bool
has_1_clause(const struct clutter *c)
{
  size_t i;
  for(i=0; i<c->live_data->size; i++) {
    if(clause_all_onep(c->clause_ctx,
		       c->data[c->live_data->indices[i]]))
      return true;
  }
  return false;
}


/* check if CLAUSE is a subset of any clause in C, or already present. */
bool
clause_subsumes(const struct clause *clause, struct clutter *c)
{
  size_t i;
  for(i=0; i<c->live_data->size; i++) {
    if(clause_contained(c->clause_ctx, clause, c->data[c->live_data->indices[i]]))
      return true;
  }
  return false;
}

/* check if CLAUSE is a superset of any clause in c */
bool
clause_subsumed(const struct clause *clause, struct clutter *c)
{
  size_t i;

  for(i=0; i<c->live_data->size; i++) {
    if(clause_contains(c->clause_ctx, clause, c->data[c->live_data->indices[i]]))
      return true;
  }
  return false;
}

#define CLUTTER_DATA_GROWTH_INCREMENT 128
/* store clause in clutter. */
struct clutter*
clutter_store_clause_no_check(/*@returned@*/struct clutter *c,
			      /*@kept@*/struct clause *clause)
{
  size_t i;
  assert(c->live_data->size==c->numclauses);
  if(c->data_len<=c->numclauses) {
    /* extend */
    struct clause **tmp1 = realloc(c->data,
				   sizeof(struct clause*)
				   *(CLUTTER_DATA_GROWTH_INCREMENT+c->data_len));
    struct live_data *tmp2 = realloc(c->live_data,
				     sizeof(struct live_data) +
				     sizeof(size_t) * (CLUTTER_DATA_GROWTH_INCREMENT+c->data_len));
    if(tmp1==NULL || tmp2==NULL)
      {
	NFREE(tmp1);
	free_live_data(tmp2);
	return NULL;
      }

    c->data = tmp1;
    c->live_data = tmp2;
    c->data_len+=CLUTTER_DATA_GROWTH_INCREMENT;
  }
  c->data[c->numclauses] = clause;
  c->live_data->indices[c->numclauses] = c->numclauses;
  c->numclauses++;
  c->live_data->size=c->numclauses;

  /* count contribution of clause to frequencies */
  for(i=0; i<c->clause_ctx->numbits; i++)
    c->live_data->varfreq[i] += clause_ref(clause, i) == true ? 1 : 0;

  maybe_report(c);
  
  return c;
}

/* store clause in clutter, which must neither be a superset not a subset of any
   clause in the clutter */
struct clutter*
clutter_store_clause(/*@returned@*/struct clutter *c,
		     /*@kept@*/struct clause *clause)
{
  if(clause_subsumed(clause,c)) {
    fprintf(stderr, "Clause %p not strong enough for clutter, cannot store.\n",
	    clause);
    assert(0);
    return NULL;
  } else if(clause_subsumes(clause,c)) {
    fprintf(stderr, "Clause %p would require prior subset removal in clutter, cannot store.\n",
	    clause);
    /* why no assert(0) here? 
       Shouldn't we force people to use c_s_or_drop_clause()? */
    return NULL;
  } else {
    return clutter_store_clause_no_check(c,clause);
  }
}

/* store clause in clutter unless it was too weak,
   i.e. subsumed by some clause in c. */
struct clutter*
clutter_store_or_drop_clause(/*@returned@*/struct clutter *c,
			     /*@only@*/struct clause *clause)
{
  if(clause_subsumed(clause,c)) {
    fprintf(stderr, "Clause %p not strong enough for clutter, dropping it.\n",
	    clause);
    free_clause(c->clause_ctx,clause);
    return c;
  } else if(clause_subsumes(clause,c)) {
    fprintf(stderr, "Clause %p would require prior subset removal in clutter, dropping it.\n",
	    clause);
    free_clause(c->clause_ctx,clause);
    return c;
  } else {
    return clutter_store_clause_no_check(c,clause);
  }
}

void
print_clutter(FILE *fp, const struct clutter *c, const char *header)
{
  size_t i;
  
  if(header!=NULL)
    fprintf(fp, "c %s\n", header);

  fprintf(fp, "p dnf %zu %zu\n", c->clause_ctx->numbits, c->live_data->size);
  for(i=0; i<c->live_data->size; i++) 
    print_clause(fp, c->clause_ctx,
		 c->data[c->live_data->indices[i]], DEFAULT_CLAUSE_PRINT_FORMAT);
}


/* restrict A by selecting only clauses with 0 entries for VARIABLE */
static void
push_live_data_stack(struct clutter *A)
{
  struct live_data *tmp = make_live_data(A->data_len, A->clause_ctx->numbits);
  if(tmp) {
    tmp->size = A->live_data->size;
    /* copy current live variable info */
    memcpy(tmp->indices, A->live_data->indices,
	   sizeof(size_t) * A->live_data->size);
    /* copy current frequency info */
    memcpy(tmp->varfreq, A->live_data->varfreq,
	   sizeof(size_t) * A->clause_ctx->numbits);
    
    tmp->prev = A->live_data;
    A->live_data = tmp;
  } else {
    assert(0);
  }
}
static void
pop_live_data_stack(struct clutter *A)
{
  struct live_data *prev = A->live_data->prev;
  free_live_data(A->live_data);
  A->live_data = prev;
}

static void
correct_frequencies_for_dropped_clause(struct clutter *A, const struct clause *c)
{
  size_t bit;
  const struct clause *mask = A->clause_ctx->mask;

  for(bit=0; bit<A->clause_ctx->numbits; bit++) {
    if(bit_unmasked(mask,bit) && clause_ref(c,bit))
      A->live_data->varfreq[bit]--;
  }
}

void
restrict_clutter_delete(struct clutter *A, size_t variable)
{
  size_t src,dst;

  push_live_data_stack(A);
  clause_ctx_mask_bit(A->clause_ctx,variable);

  {
    for(src=0,dst=0; src<A->live_data->size; src++) {
      struct clause *src_clause = A->data[A->live_data->indices[src]];
      if(clause_ref(src_clause, variable) == false) {
	/* has 0 here, keep it */
	A->live_data->indices[dst] = A->live_data->indices[src];
	dst++;
      } else {
	/* has 1, drop it, correcting frequencies */
	correct_frequencies_for_dropped_clause(A, src_clause);
      }
    }
    A->live_data->size = dst;
  }
}

/* restrict A by ignoring index VARIABLE in all clauses and removing resulting supersets */
void
restrict_clutter_contract(struct clutter *A, size_t variable)
{
  size_t src,dst;

  push_live_data_stack(A);
  clause_ctx_mask_bit(A->clause_ctx,variable);

  /* sort 1-index clauses to be at the start of live variables list, zeros at the end.
   */
#define SWAP_INDICES(a,b) do {				\
    size_t tmp;						\
    tmp = (a); (a) = (b); (b) = tmp;			\
  } while(false)
  
  src=0; dst=A->live_data->size;
  while(src<dst) {
    while(src<A->live_data->size) {
      if(clause_ref(A->data[A->live_data->indices[src]], variable)==false)
	break;
      src++;
    }
    /* src is now the leftmost index where we see a 0 for variable */
    while(dst>src) {
      /* cannot be done as dst-- because we rely on dst not underflowing for
	 later tests */
      if(clause_ref(A->data[A->live_data->indices[--dst]], variable)==true)
	break;
    }
    /* dst is now the rightmost index where we see a 1 for variable */
    if(src<dst) {
      /* not sorted yet */
      SWAP_INDICES(A->live_data->indices[src], A->live_data->indices[dst]);
      src++;
    }
  };

  /* now src is behind the last 1-clause */
  {
    size_t num_1_clauses = src;
    size_t old_size = A->live_data->size;
    A->live_data->size = num_1_clauses; /* these are guaranteed to survive */
    
    for(src=src; src < old_size; src++) {
      struct clause *src_clause = A->data[A->live_data->indices[src]];
      
      if(clause_subsumed(src_clause, A)) {
	/* correct frequency count */
	correct_frequencies_for_dropped_clause(A, src_clause);
      } else {
	A->live_data->indices[A->live_data->size++] = A->live_data->indices[src];
      }
    }
  }
}

/* undo last restriction on VARIABLE on A */
void
unrestrict_clutter(struct clutter *A, size_t variable)
{
  clause_ctx_unmask_bit(A->clause_ctx,variable);
  pop_live_data_stack(A);
}
