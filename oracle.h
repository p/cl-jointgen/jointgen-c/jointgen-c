/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef ORACLE_H
#define ORACLE_H

#include "clause.h"

struct oracle;

enum oracle_answer {
  ORACLE_FALSE,
  ORACLE_TRUE,
  ORACLE_FAILED
};

enum oracle_answer
query_oracle(struct oracle* oracle, const struct clause* c);

  
typedef enum oracle_answer(*oracle_fun)(const struct oracle* o,
					const struct clause *c);

typedef struct clause *(*clause_min_fun)(struct oracle* oracle,
					 struct clause *c);
typedef struct clause *(*clause_max_fun)(struct oracle* oracle,
					 struct clause *c);

/* clause_gen_fun is called strategically to return clauses that the oracle things
   should be added to the problem. It should return a fresh clause, or NULL. */
typedef struct clause *(*clause_rand_gen_fun)(const struct oracle* oracle);
					 
typedef void (*oracle_destructor_fun)(void *closure);

struct oracle {
  oracle_fun o;
  void* closure;
  size_t dim;
  struct clause_ctx *ctx;
  oracle_destructor_fun destructor;
  clause_min_fun minfun;
  clause_max_fun maxfun;
  size_t num_invocations;
  
};

/* Create an oracle instance accepting clauses of dimension DIM that invokes F
   to check membership.
   CLOSURE is an opaque pointer that will be passed to the oracle on every
   invocation.
   CTX, if non-NULL, is the clause-context to be used for the clutters in
   the joint-generation algorithm. It will be dereferenced once by free_oracle.
   DESTRUCTOR will be called to destroy resources associated with the oracle instance
   when free_oracle is called. It may be NULL.
   MINFUN and MAXFUN are specialized clause minimization functions; they may be NULL
   in which case default_clause_min_fun and default_clause_max_fun will be used */
   
struct oracle*
make_oracle(oracle_fun f, size_t dim, void* closure,
	    struct clause_ctx *ctx,
	    oracle_destructor_fun destructor,
	    clause_min_fun minfun, clause_max_fun maxfun);

size_t
oracle_get_num_invocations(const struct oracle *o);

/* maybe we should support a user-defined closure cleanup ?*/
void
free_oracle(struct oracle *o);

enum oracle_answer
invoke_oracle(const struct oracle* oracle, const struct clause* c);

/* destructively: */
void
minimize_clause(struct oracle *oracle, struct clause *c);
void
maximize_clause(struct oracle *oracle, struct clause *c);
  



struct clause*
default_clause_min_fun(struct oracle* oracle, struct clause *c);

struct clause*
default_clause_max_fun(struct oracle *oracle, struct clause *c);

struct clause*
default_clause_rand_gen_fun(struct oracle *oracle);


/*** Standard oracle implementations ***/
#include "clutter.h"
/* hypergraph dualization, where clutter represents hyperedges */
struct oracle*
make_hypergraph_edge_oracle(/*@only@*/struct clutter *c);

/* permutation matrices as hypergraph edges */
struct oracle*
make_permutation_oracle(size_t dim);



#endif
