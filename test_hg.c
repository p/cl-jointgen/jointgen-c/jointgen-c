/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "dimacs.h"
#include "oracle.h"
#include "jg.h"
#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char **argv)
{
  FILE *fp;
  switch(argc) {
  case 1: fp=stdin;
    break;
  case 2: fp=fopen(argv[1],"r");
    break;
  default:
    fp=NULL;
  }
  if(fp==NULL) {
    fprintf(stderr, "Usage: %s [DNF file]\n", argv[0]);
    return EXIT_FAILURE;
  } else {
    struct clutter *c = read_dimacs_dnf(fp,NULL);
    if(fp!=stdin)
      fclose(fp);

    print_clutter(stdout, c, "Hypergraph input");

    if(c!=NULL) {
      struct oracle *o = make_hypergraph_edge_oracle(c);
      struct jg_prob *p = make_jg_prob(o);
      joint_generation(p,stdout);
      print_jg_prob(stdout, p ,"Hypergraph and transversal");
      free_jg_prob(p);
    }
	
    return EXIT_SUCCESS;
  }
}
    
