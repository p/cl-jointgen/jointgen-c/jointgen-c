JOINTGEN-C -- A C library implementing Fredman-Khachiyan's algorithm
====================================================================

To compile the jointgen code you will need a C compiler and the [libgmp](http://gmplib.org/) library. 

If you got the code by checking out the
[jointgen-c git repository](git://git.code.sf.net/p/cl-jointgen/jointgen-c/jointgen-c.git)
at the
[sourceforge project page](https://sourceforge.net/projects/jointgen-c.cl-jointgen.p/)
you will also need the [GNU automake/autoconf/libtool](http://en.wikipedia.org/wiki/GNU_build_system) chain of programs.

Run the `autogen.sh` script to complete the build environment:
	`sh autogen.sh`
Then run the configure script:
	`sh ./configure`

Then run 
	`make test_hg`
to compile the hypergraph dualization example code. It takes as input a file in [DIMACS DNF format](ftp://dimacs.rutgers.edu/pub/challenge/satisfiability/doc/satformat.dvi).

For documentation of the API start by reading
[duality_check.h](duality_check.h) and [jg.h](jg.h).  The data
structures for clauses and clutters are in [clause.h](clause.h) and
[clutter.h](clutter.h). Oracles must simply implement the interface defined in [oracle.h](oracle.h), see [o-hg.c](o-hg.c) for an example.

