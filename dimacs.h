/*
    Copyright (C) 2010-2013 Utz-Uwe Haus <uhaus@ethz.ch>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef DIMACS_H
#define DIMACS_H

#include "config.h"
#include "clutter.h"
#include <stdio.h>

/* Read a DIMACS-style DNF formula from SRC and return a fresh clutter structure.
   If CTX is non-NULL, re-use the clause context CTX for the new clutter, otherwise
   allocate a fresh one.
   Returns NULL on all hard errors, like reading errors or ctx mismatch */
/*@null@*//*@only@*/
struct clutter*
read_dimacs_dnf(FILE* src, struct clause_ctx *ctx);

#endif
